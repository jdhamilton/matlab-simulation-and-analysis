% Array Simulation, 2D

% Todo: loop over field points, not elements
% Progress bar
% Add directivity and obliquity
% Reference pressure (single element at fixed distance)
clear

user_root = 'C:\Users\JamieHamilton\';


%% Load parameter file
param_dir = 'C:\Users\JamieHamilton\Google Drive\Emerge\SimulationsJH\ParameterFiles\';
param_file = 'ParamsArray3D_SingleMap';
%param_file = 'ParamsArray3D_MultiMap';
%param_file = 'ParamsArray3D_ApodizeAngle';

%param_file = 'ParamsArray3D_SubArrayGratingJam';
%param_file = 'ParamsArray3D_LargeElements';
%param_file = 'ParamsArray3D_LargeElements_CircularArrangement';
%param_file = 'ParamsArray3D_CircularArrangement';



run([param_dir,param_file]);


% Amplitude only
%phs_foc = exp(1j*angle(phs_foc));


% Set maximum amplitude to 1
mx_amp = max(abs(phs_foc(:)));
phs_foc = phs_foc / mx_amp;


% Turn on force calculation
force_calc = 0;

% Apply non-linear loss (post acoustic field calculation)
nonlinear_on = 1;

% If aperture functions are 2D, make 1D for compatibility with non-grid
% arrangements
if ~isvector(phs_foc)
    phs_foc = phs_foc(:);
    apod = apod(:);
    
    % Linearize array coordinates
    xn = ones(Ne,1) * xn;
    yn = yn.' * ones(1,Ne);
    xn = xn(:);
    yn = yn(:);

    % Element location in image pixels
    xn_pix = xn / d_pix + Nx/2 ;
    yn_pix = yn / d_pix + Ny/2;


    
    Ne = length(xn);
    
    
end




%% Create Field
rf_sum = zeros(Nx,Ny,Nz);
tic



% Set up waitbar
h = waitbar(0,'Calculating Acoustic Field');

for elem_idx = 1:Ne
    
    % Update status bar
    waitbar(elem_idx/Ne,h);
    
    
    % distance from element to field point
    delta_y = Y-yn(elem_idx);
    
    
    
    
    % distance from element to field point
    delta_x = X-xn(elem_idx);
    
    
    % Distance from element to all field points
    d = sqrt( delta_x.^2 + delta_y.^2 + Z.^2);
    
    % Distance in x,y plane
    d_xy = sqrt( delta_x.^2 + delta_y.^2);
    
    
    % Angle from element to all field points
    phi = atan2(delta_y,delta_x);
    theta = atan2(d_xy,Z);
    
    % Look up directivity and obliquity
    antenna_reduction = interp2(directivity.phi,directivity.theta,directivity.mag,phi,theta);
    %antenna_reduction = 1;
    
    % Ampltidue scaling factor (1/r radiator scaled)
    A_pa = elem_pres_pa./ d * elem_pres_dist_m;
    
    % Calculate amplitude scaling from thermoviscous attenuation
    A_atten = 10.^(-atten_dB_m * d / 20);
    
    
    % Phase change to all elements
    phs_d = exp(-1j*k*d);
    
    
    % =========================================
    % Total phase
    phs_tot = phs_d .* phs_foc(elem_idx);
    
    % Total amplitude
    A_tot = apod(elem_idx) .* A_pa .* antenna_reduction .* A_atten ;
    
    % RF pressure, complex
    rf = A_tot .* phs_tot;
    
    rf_sum = rf_sum + rf;
    
    
end

close(h)
toc

%% Report out information

% Focus distance
focus_d = sqrt(foc_x(1)^2 + foc_y(1).^2 + foc_z(1)^2);

% Array size
array_L = max(xn(:)) - min(xn(:));


% F/#, assume square
F_num = focus_d/array_L;

fprintf('\nSimulation parameters\n====================================\n');
fprintf('Number of elements = %d x %d\n',Ne, Ne);
fprintf('Element spacing = %0.2f cm\n',de * 100);
fprintf('Element size = %0.2f cm\n',elem_sz_mm/10);
for idx = 1:N_foc
    fprintf('Focus position (x,y,z) = %0.2f,%0.2f,%0.2f\n',foc_x(idx)*100,foc_y(idx)*100,foc_z(idx)*100);
end

fprintf('F/# = %0.2f\n',F_num);
fprintf('Frequency = %0.1f kHz\n',f/1000)
fprintf('Single element pressure (@ %0.2fcm) = %0.1f dB\n',elem_pres_dist_m*100, elem_pres_dB)





%% Display results
pres_mag = abs(rf_sum);
pres_mag_dB = 20*log10(pres_mag/ref_Pa);


% Apply non-linear loss ------------------------------------
if nonlinear_on
    
    % Set-up variables
    fieldpts.z_m = z;
    fieldpts.x_m = x;
    fieldpts.y_m = y;
    
    % Apply non-linear loss
    pres_mag_nl = nonlinearloss(pres_mag,fieldpts,2,1);
    
    pres_tmp = pres_mag_nl;
else
    
    pres_tmp = pres_mag;
    
end


% Pressure, Pa ====================
mx = max(pres_tmp(:));
scl_rng = [0 mx/2];


% Show panel of image planes =============================
N_panel = 20;

if N_panel > Nz
    N_panel = Nz;
end


slice_sel = round([1:N_panel] * Nz/N_panel);
im_panel(pres_tmp(:,:,slice_sel),scl_rng);

% Show slices of beam ====================================


% y,z slice
mid_x = round(Nx/2);
pres_slice = squeeze(pres_tmp(:,mid_x,:));
figure
imagesc(pres_slice,scl_rng)
%axis('image')
colormap(bone)
hold on
plot(zeros(1,Ne),yn_pix,'ob','linewidth',3)

% Set axis labels
ca = get(gca);
set(gca,'Xtick',z_tic)
set(gca,'Ytick',y_tic)
set(gca,'XtickLabel',z_label_cm)
set(gca,'YtickLabel',y_label_cm)

xlabel('Z position (cm)');
ylabel('Y position (cm)');
colorbar
title('Acoustic pressure amplitdue, y,z slice')

% x,z slice
mid_y = round(Ny/2);
pres_slice = squeeze(pres_tmp(mid_y,:,:));
figure
imagesc(pres_slice,scl_rng)
%axis('image')
colormap(bone)
hold on
plot(zeros(1,Ne),xn_pix,'ob','linewidth',3)

% Set axis labels
ca = get(gca);
set(gca,'Xtick',z_tic)
set(gca,'Ytick',x_tic)
set(gca,'XtickLabel',z_label_cm)
set(gca,'YtickLabel',x_label_cm)

xlabel('X position (cm)');
ylabel('Z position (cm)');
colorbar
title('Acoustic pressure amplitdue, x,z slice')

% 3D surface visualization ================================
im = zeros(size(pres_tmp));
for idx = 1:Nz
    dsub = (pres_tmp(:,:,idx));
    mx = max(dsub(:));
    im(:,:,idx) = dsub / mx;
    
end


d_mag = im;
iso_val = 0.5;
iso_surf = isosurface(X,Y,Z,d_mag,iso_val);
figure
hpatch= patch(iso_surf);
isonormals(X,Y,Z,d_mag,hpatch)
%isocolors(target_X0_m,target_Y0_m,target_Z0_m,d_mag,hpatch)

hpatch.FaceColor = 'red';

hpatch.EdgeColor = 'none';
daspect([1,4,4])
view([-65,20])
axis image
camlight left;
lighting gouraud



for foc_idx = round(N_foc/2) %1:N_foc
    
    % On axis pressure ==============================
    
    foc_x_cur = foc_x(foc_idx);
    foc_y_cur = foc_y(foc_idx);
    foc_z_cur = foc_z(foc_idx);
    
    
    % Find index of lateral positon of focus
    [foo,x_use_idx] = min(abs(x-foc_x_cur));
    [foo,y_use_idx] = min(abs(y-foc_y_cur));
    
    % Find index for focus
    [foo,z_foc_idx] = min(abs(z-foc_z_cur));
    
    % 2D interpolation of slices to create line through focus ---------------------
    slope_y = foc_y_cur/foc_z_cur;
    slope_x = foc_x_cur/foc_z_cur;
    cntr_line = zeros(1,Nz);
    for idx = 1:Nz
        cur_z = z(idx);
        cur_x = slope_x * cur_z;
        cur_y = slope_y * cur_z;
        cntr_line(idx) = interp2(x,y,pres_tmp(:,:,idx),cur_x,cur_y);
    end
    
    
    figure
    plot(z*100,cntr_line,'linewidth',2)
    hold
    plot(z(z_foc_idx)*100,cntr_line(z_foc_idx),'rd','linewidth',2)
    grid
    title(sprintf('On axis pressure, focus = (%0.1f,%0.1f,%0.1f)cm',foc_x_cur*100, foc_y_cur*100, foc_z_cur*100))
    ylabel('Pressure Pa')
    xlabel('Z Distance (cm)')
    
    
    
    %% Beam width meaurement -------------------------------------
    
    % Identify plane containing focus
    foc_plane = pres_tmp(:,:,z_foc_idx);
    figure('position', [1106 86 697 559])
    subplot(221)
    imagesc(foc_plane)
    colorbar
    set(gca,'Xtick',x_tic)
    set(gca,'Ytick',y_tic)
    set(gca,'XtickLabel',x_label_cm)
    set(gca,'YtickLabel',y_label_cm)
    xlabel('X position (cm)');
    ylabel('Y position (cm)');
    title(sprintf('Pressure in focal plane \nat %0.2f cm',z(z_foc_idx)*100));
    
    subplot(222)
    contour(foc_plane,6)
    colorbar
    set(gca,'Xtick',x_tic)
    set(gca,'Ytick',y_tic)
    set(gca,'XtickLabel',x_label_cm)
    set(gca,'YtickLabel',y_label_cm)
    xlabel('X position (cm)');
    ylabel('Y position (cm)');
    grid
    colormap('jet')
    title(sprintf('Pressure in focal plane \nat %0.2f cm',z(z_foc_idx)*100));
    
    
    
    % Create cross plots
    y_line = foc_plane(:,x_use_idx);
    x_line = foc_plane(y_use_idx,:);
    
    % Calculate FWHM --------------------------------
    mx_x = max(x_line);
    
    % Interpolate function to avoid quantization error
    R = 10;
    x_up = [x_start:d_pix/R:x_end];
    x_line_up = interp1(x,x_line,x_up);
    
    pts_above = sum(x_line_up >= (mx_x/2));
    FWHM_x_mm = pts_above * d_pix / R * 1000;
    
    y_up = [y_start:d_pix/R:y_end];
    y_line_up = interp1(y,y_line,y_up);
    
    pts_above = sum(y_line_up >= (mx_x/2));
    FWHM_y_mm = pts_above * d_pix / R * 1000;
    
    
    subplot(223)
    plot(y*100,y_line,'b','linewidth',2)
    grid
    title(sprintf('Line along y = %0.1f cm\nFWHM = %0.2f mm',x(x_use_idx)*100,FWHM_y_mm))
    xlabel('Distance (cm)')
    ylabel('Pressure (Pa)')
    
    subplot(224)
    plot(x*100,x_line,'b','linewidth',2)
    grid
    title(sprintf('Line along x = %0.1f cm\nFWHM = %0.2f mm',y(y_use_idx)*100,FWHM_x_mm))
    xlabel('Distance (cm)')
    ylabel('Pressure (Pa)')
    
    
    % Theoretical beamwidth
    FWHM_theory = F_num * lam * 1000;
    fprintf('\nF number = %0.2f\n',F_num);
    fprintf('Wavelength = %0.2f mm\n',lam*1000);
    fprintf('Theoretical FWHM = %0.2f mm\n',FWHM_theory);
    
    
end

%% Aperture function if
phs_show = phs_foc;
if min(size(phs_show)) > 1
figure('position',[764 555 560 231])
subplot(121)
imagesc(abs(phs_show))
colorbar
title('Aperture amplitude')
axis('image')


subplot(122)
imagesc(angle(phs_show))
colorbar
title('Aperture phase')
axis('image')
end


%% Acoustic radiation force with varying size disk
if force_calc

[f_Dia,Dia_disk_m] = RadForceDisk(foc_plane,d_pix);

figure
plot(Dia_disk_m*100,f_Dia,'linewidth',2)
grid
xlabel('Disk diameter (cm)')
ylabel('Force (N)')
yaxis(0,0.05)
xaxis(0,10)
end





